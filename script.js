/* Ответ на теоретический вопрос:

Методы объекта это функции описанные внутри объекта, которые задают его "поведение".
Существуют для того чтобы управлять свойствами самого объекта, а также выполнять какие то действия для пользователя, посредством алгоритма описанного в данной функции.*/

function createNewUser() {
    let firstName;
    let lastName;

    do{
        firstName = prompt("Enter your first name, please");
        lastName = prompt("Enter your last name, please");
    }while(firstName === "" || lastName === "");

    const newUser = {
        firstName,
        lastName,
        setFirstName(fName) {
            Object.defineProperty(this, "firstName", {
                value: fName,
            });
        },
        setLastName(lName) {
            Object.defineProperty(this, "lastName", {
                value: lName,
            });
        },
        getLogin: function () {
            let login = firstName.charAt(0) + lastName;
            return login.toLowerCase();
        },
    }
    Object.defineProperty(newUser, "firstName", {
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser, "lastName", {
        writable: false,
        configurable: true,
    });

    return newUser;
}

const myUser = createNewUser();
console.log(myUser.getLogin());

myUser.firstName = "TEST1"; //Пытаемся перезаписать имя напрямую
myUser.lastName = "TEST1"; //Пытаемся перезаписать имя напрямую
console.log(myUser);

myUser.setFirstName("TEST2"); // Пытаемся перезаписать имя через метод
myUser.setLastName("TEST2"); // Пытаемся перезаписать фамилию через метод
console.log(myUser);